
Getting Started
---------------

I have added `shell.nix` (and `nix/`), so just enter into the `nix-shell` &mdash; that will fetch `rustc 1.62.0` and `cargo 1.62.0`.

Then, the following should work:

```bash
$ cargo run -- ./transactions.csv  >> ledgers.csv
...

$ cargo test -- --nocapture
...
```

Outline of the code
------------------

There are basically 3 Rust modules:

- `parser.rs` - parses the CSV files creating `RawTransaction` which then is converted into `Transaction`, and then sends using `sender` channel.
- `engine.rs` - defines `PaymentEngine` which is responsible for reading transactions from `receiver` channel and managing ledgers.
- `ledger.rs` - defines `Ledger`, various transaction types and various operations. The design of ledger transaction types encodes the pre-conditions for each transaction so that impossible scenarios do not arise at runtime, e.g `resolve` transaction can be processed only if there is a corresponding `dispute` transaction, and so on. Of course, this depends on my understanding of the problem. I've listed my assumptions in the next section.

Apart from that, `main.rs` just initializes the logger, creates channel and invokes the engine.

Assumptions
-----------

I had couple of doubts as to which kinds of transactions can be disputed (hence resolved and chargebacked) and how each of them should be handled. I tried researching about it, though couldn't come to any conclusion that can be implemented without deviating from the wordings in the coding-challenge. So I assumed the simplest case: both `deposit` and `withdrawal` transactions can be disputed and I handled both in the same way.

```rust
// see dispute() in ledger.rs for more detail and comments
NormalTransaction::Deposit(amount) => {
    self.available -= amount;
    self.held += amount;
}
NormalTransaction::Withdrawal(amount) => {
    self.available -= amount;
    self.held += amount;
}
```

Also, it's based on this understanding or rather assumption, I designed the ledger transaction types (defined in `ledger.rs`).

Testing
-------

There are some basic unit tests as well. More unit tests and e2e can be added later.
