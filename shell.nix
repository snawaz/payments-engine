{ source ? import ./nix/sources.nix,
  system ? builtins.currentSystem,
  # crossSystem? null,
  overlays ? [ ],
}:
let
  inherit (source) nixpkgs;


  pkgs = import nixpkgs {
    inherit system; # crossSystem;
  };

  inherit (pkgs) buildPackages lib nixpkgs-fmt;
  inherit (buildPackages) stdenv;

in
rec {
  shell = pkgs.mkShell {
    nativeBuildInputs = [
      pkgs.rustc
      pkgs.cargo
      pkgs.libiconv
      pkgs.rustfmt
    ];
    shellHooks = ''
    '';
  };
}
