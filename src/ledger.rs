use std::collections::hash_map::{Entry, HashMap};

use log::{debug, error, warn};

// here goes the simple and sweet transactions
#[derive(Debug, Clone)]
enum NormalTransaction {
    Deposit(f64),
    Withdrawal(f64),
}

// the design of following three transaction types ensures that the
// prerequisite for each transaction are checked by the compiler itself, i.e
//
//  - only normal (deposit and withdraw) transactions can be disputed.
//  - only dispute transactions can be resolved.
//  - only dispute transactions can be chargebacked.
//
// in other words, the type-system itself encodes these requirements so that
// we do not have to worry about them too much at the runtime.
//
// also, these types are also used in the ledger operations in such a way that
// a normal transaction isn't disputed twice or more; likewise, a disputed transaction
// isn't resolved twice or more, or chargebacked twice or more.

#[derive(Debug, Clone)]
struct DisputeTransaction(NormalTransaction); // only normal can be disputed

#[derive(Debug, Clone)]
struct ResolveTransaction(DisputeTransaction); // only dispute can be resolved

#[derive(Debug, Clone)]
struct ChargebackTransaction(DisputeTransaction); // only dispute can be chargebacked

// here goes the ledger transaction consisting of all possible transactions
#[derive(Debug, Clone)]
enum LedgerTransaction {
    Normal(NormalTransaction),
    Dispute(DisputeTransaction),
    Resolve(ResolveTransaction),
    Chargeback(ChargebackTransaction),
}

// well, a ledger is basically a client's account that manages a record of all
// the transactions and funds.
#[derive(Debug)]
pub struct Ledger {
    id: u16,
    transactions: HashMap<u32, LedgerTransaction>,
    available: f64,
    held: f64,
    locked: bool,
}

impl Ledger {
    pub fn new(id: u16) -> Ledger {
        Ledger {
            id: id,
            transactions: HashMap::new(),
            available: 0.0,
            held: 0.0,
            locked: false,
        }
    }

    // All these functions {deposit, withdraw, dispute, resolve, chargeback} returns
    // a boolean value indicating whether the operation succeeded or not, even though
    // the caller of these functions ignore the returned value currently. In a real
    // service, we could return a more useful value instead, e.g
    //      -> Result<f64, LedgerError>
    // where the Ok value is the available balance and the Err value is a well-designed
    // error as per need.

    pub fn deposit(&mut self, tx: u32, amount: f64) -> bool {
        debug!("deposit: {}, {}", tx, amount);
        if self.locked {
            warn!("cannot deposit as account {} is locked", self.id);
            return false;
        } else if amount <= 0.0 {
            warn!(
                "cannot deposit as amount {} isn't positive non-zero value",
                amount
            );
            return false;
        }
        match self.transactions.entry(tx) {
            Entry::Occupied(_) => {
                error!(
                    "transaction with id {} already exist for ledger {}",
                    tx, self.id
                );
            }
            Entry::Vacant(entry) => {
                self.available += amount;
                entry.insert(LedgerTransaction::Normal(NormalTransaction::Deposit(
                    amount,
                )));
                return true;
            }
        }
        return false;
    }

    pub fn withdraw(&mut self, tx: u32, amount: f64) -> bool {
        debug!("withdrawal: {}, {}", tx, amount);
        if self.locked {
            warn!("cannot withdraw as account {} is locked", self.id);
            return false;
        } else if amount <= 0.0 {
            warn!(
                "cannot withdraw as amount {} isn't positive non-zero value",
                amount
            );
            return false;
        } else if self.available < amount {
            warn!("cannot withdraw as account {} available balance is less than the amount to be withdrew", self.id);
            return false;
        }
        match self.transactions.entry(tx) {
            Entry::Occupied(_) => {
                error!(
                    "transaction with id {} already exist for ledger {}",
                    tx, self.id
                );
            }
            Entry::Vacant(entry) => {
                self.available -= amount;
                entry.insert(LedgerTransaction::Normal(NormalTransaction::Withdrawal(
                    amount,
                )));
                return true;
            }
        }
        return false;
    }

    pub fn dispute(&mut self, tx: u32) -> bool {
        debug!("dispute: {}", tx);
        if self.locked {
            warn!("cannot dispute as account {} is locked", self.id);
            return false;
        }
        if let Entry::Occupied(mut entry) = self.transactions.entry(tx) {
            if let LedgerTransaction::Normal(normal) = entry.get() {
                match normal {
                    // the spec isn't clear about the dispute scenarios explicitly.
                    // e.g whether it should apply to Deposit transactions only, or
                    // Withdrawal only, or both, and whether the available/held should be
                    // modified in the same way in both cases or not.
                    //
                    // in the following code, I'm carrying out the same computation in
                    // both cases. if this assumption is wrong, this can be quickly fixed,
                    // as it's a matter of domain-specific knowledge, there is not much technical
                    // challenge in it.
                    NormalTransaction::Deposit(amount) => {
                        self.available -= amount;
                        self.held += amount;
                    }
                    NormalTransaction::Withdrawal(amount) => {
                        self.available -= amount;
                        self.held += amount;
                    }
                }

                // let's wrap the referred Deposit/Withdrawal transaction in a Dispute transaction
                // to ensure that a single Deposit/Withdrawal transaction isn't disputed more
                // than once.
                entry.insert(LedgerTransaction::Dispute(DisputeTransaction(
                    normal.clone(),
                )));
                return true;
            }
        }
        return false;
    }

    pub fn resolve(&mut self, tx: u32) -> bool {
        debug!("resolve: {}", tx);
        if self.locked {
            warn!("cannot resolve as account {} is locked", self.id);
            return false;
        }
        if let Entry::Occupied(mut entry) = self.transactions.entry(tx) {
            if let LedgerTransaction::Dispute(dispute) = entry.get() {
                // reversing the computation done in dispute()
                match dispute.0 {
                    NormalTransaction::Deposit(amount) => {
                        self.available += amount;
                        self.held -= amount;
                    }
                    NormalTransaction::Withdrawal(amount) => {
                        self.available += amount;
                        self.held -= amount;
                    }
                }

                // let's wrap the Dispute transaction in a Resolve transaction
                // so that it is neither disputed nor resolved again
                entry.insert(LedgerTransaction::Resolve(ResolveTransaction(
                    dispute.clone(),
                )));
                return true;
            }
        }
        return false;
    }

    pub fn chargeback(&mut self, tx: u32) -> bool {
        debug!("chargeback: {}", tx);
        if self.locked {
            warn!("cannot chargeback as account {} is locked", self.id);
            return false;
        }
        if let Entry::Occupied(mut entry) = self.transactions.entry(tx) {
            if let LedgerTransaction::Dispute(dispute) = entry.get() {
                self.locked = true;
                match dispute.0 {
                    NormalTransaction::Deposit(amount) => {
                        self.held -= amount;
                    }
                    NormalTransaction::Withdrawal(amount) => {
                        self.held -= amount;
                    }
                }
                // let's wrap the Dispute transaction in a Chargeback transaction
                // so that it is neither disputed nor chargebacked again
                entry.insert(LedgerTransaction::Chargeback(ChargebackTransaction(
                    dispute.clone(),
                )));
                return true;
            }
        }
        return false;
    }

    // some friendly queries about the ledger

    pub fn id(&self) -> u16 {
        self.id
    }

    pub fn is_locked(&self) -> bool {
        self.locked
    }

    pub fn available_balance(&self) -> f64 {
        self.available
    }

    pub fn held_balance(&self) -> f64 {
        self.held
    }

    pub fn total_balance(&self) -> f64 {
        self.available + self.held
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_new() {
        let ledger = Ledger::new(1u16);

        assert_eq!(ledger.id(), 1u16);
        assert_eq!(ledger.is_locked(), false);
        assert_eq!(ledger.available_balance(), 0.0);
        assert_eq!(ledger.held_balance(), 0.0);
        assert_eq!(ledger.total_balance(), 0.0);
    }

    #[test]
    fn test_normal_transactions() {
        let mut ledger = Ledger::new(1u16);

        assert_eq!(ledger.deposit(1, 100.0), true);
        assert_eq!(ledger.withdraw(2, 64.0), true);
        assert_eq!(ledger.deposit(3, 200.0), true);
        assert_eq!(ledger.withdraw(4, 84.0), true);
        assert_eq!(ledger.withdraw(5, 17.0), true);

        assert_eq!(ledger.deposit(1, 100.0), false);
        assert_eq!(ledger.withdraw(2, 64.0), false);

        assert_eq!(ledger.is_locked(), false);
        assert_eq!(
            ledger.available_balance(),
            (100.0 - 64.0 + 200.0 - 84.0 - 17.0)
        );
        assert_eq!(ledger.held_balance(), 0.0);
        assert_eq!(ledger.total_balance(), ledger.available_balance());
    }

    #[test]
    fn test_dispute_and_resolve_transactions() {
        let mut ledger = Ledger::new(1u16);

        assert_eq!(ledger.deposit(1, 100.0), true);

        assert_eq!(ledger.dispute(1), true);
        assert_eq!(ledger.available_balance(), 0.0);
        assert_eq!(ledger.held_balance(), 100.0);

        assert_eq!(ledger.dispute(1), false); // cannot dispute more than once
        assert_eq!(ledger.available_balance(), 0.0);
        assert_eq!(ledger.held_balance(), 100.0);

        assert_eq!(ledger.resolve(1), true);
        assert_eq!(ledger.available_balance(), 100.0);
        assert_eq!(ledger.held_balance(), 0.0);

        assert_eq!(ledger.resolve(1), false); // cannot resolve more than once
        assert_eq!(ledger.available_balance(), 100.0);
        assert_eq!(ledger.held_balance(), 0.0);

        assert_eq!(ledger.is_locked(), false);
    }

    #[test]
    fn test_dispute_and_chargeback_transactions() {
        let mut ledger = Ledger::new(1u16);

        assert_eq!(ledger.deposit(1, 100.0), true);

        assert_eq!(ledger.dispute(1), true);
        assert_eq!(ledger.available_balance(), 0.0);
        assert_eq!(ledger.held_balance(), 100.0);

        assert_eq!(ledger.chargeback(1), true);
        assert_eq!(ledger.available_balance(), 0.0);
        assert_eq!(ledger.held_balance(), 0.0);

        assert_eq!(ledger.is_locked(), true);

        assert_eq!(ledger.chargeback(1), false); // cannot chargeback more than once
        assert_eq!(ledger.available_balance(), 0.0);
        assert_eq!(ledger.held_balance(), 0.0);

        assert_eq!(ledger.deposit(2, 100.0), false); // cannot dispute as ledger is locked
        assert_eq!(ledger.total_balance(), 0.0);
    }

    #[test]
    fn test_incorrect_transactions() {
        let mut ledger = Ledger::new(1u16);

        assert_eq!(ledger.deposit(1, 100.0), true);
        assert_eq!(ledger.resolve(1), false);
        assert_eq!(ledger.chargeback(1), false);

        assert_eq!(ledger.withdraw(2, 100.0), true);
        assert_eq!(ledger.resolve(2), false);
        assert_eq!(ledger.chargeback(2), false);

        assert_eq!(ledger.total_balance(), 0.0);
    }
}
