use std::{collections::hash_map::HashMap, error, io, sync::mpsc::Receiver};

use log::debug;

use crate::ledger::Ledger;
use crate::parser::Transaction;

pub struct PaymentEngine {
    ledgers: HashMap<u16, Ledger>,
}

impl PaymentEngine {
    pub fn new() -> PaymentEngine {
        PaymentEngine {
            ledgers: HashMap::new(),
        }
    }

    // this method takes the receiver part of a channel, that means
    // the sender part can read transactions from any source, be it files or
    // TCP stream, or anything else, process_all would be able to process
    // these transactions
    pub fn process_all(&mut self, receiver: Receiver<Transaction>) {
        for t in receiver {
            self.process(t);
        }
    }

    // this generates the report in a format that can be saved to a CSV file
    pub fn generate_report(
        &self,
        writer: &mut dyn io::Write,
        // note that sort_by_client parameter make testings preditable and it could be also a useful feature in general
        sort_by_client: bool,
    ) -> Result<(), Box<dyn error::Error>> {
        write!(writer, "client, available, held, total, locked\n")?;
        let mut write = |ledger: &Ledger| {
            write!(
                writer,
                "{:>6}, {:>9}, {:>4}, {:>5}, {:>6}\n",
                ledger.id(),
                ledger.available_balance(),
                ledger.held_balance(),
                ledger.total_balance(),
                ledger.is_locked()
            )
        };
        if sort_by_client {
            let mut keys: Vec<&u16> = self.ledgers.keys().collect();
            keys.sort();
            for key in keys {
                write(&self.ledgers.get(key).expect("impossible"))?;
            }
        } else {
            for ledger in self.ledgers.values() {
                write(&ledger)?;
            }
        }
        Ok(())
    }

    // process a single transaction
    fn process(&mut self, tx: Transaction) {
        debug!("Payments process invoked with: {:?}", tx);
        use Transaction::*;
        match tx {
            Deposit(t) => self.ledger(t.client).deposit(t.id, t.amount),
            Withdrawal(t) => self.ledger(t.client).withdraw(t.id, t.amount),
            Dispute(t) => self.ledger(t.client).dispute(t.id),
            Resolve(t) => self.ledger(t.client).resolve(t.id),
            Chargeback(t) => self.ledger(t.client).chargeback(t.id),
        };
    }

    // this gets or creates a new ledger (account) if it does not exist
    fn ledger(&mut self, client: u16) -> &mut Ledger {
        self.ledgers
            .entry(client)
            .or_insert_with(|| Ledger::new(client))
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::parser::read_all;
    use csv::ReaderBuilder;
    use simplelog::{ColorChoice, ConfigBuilder, LevelFilter, TermLogger, TerminalMode};
    use std::{io::Cursor, sync::mpsc::channel, thread};

    #[test]
    fn test_engine() {
        TermLogger::init(
            LevelFilter::Debug,
            ConfigBuilder::new()
                .set_thread_level(LevelFilter::Off)
                .build(),
            TerminalMode::Stderr,
            ColorChoice::Auto,
        )
        .expect("TermLogger::int() failed");

        let (sender, receiver) = channel();

        let t = thread::spawn(move || {
            let data = "
                type       , client ,  tx , amount
                deposit    ,      1 ,   1 , 100.0
                deposit    ,      1 ,   2 , 300.0
                dispute    ,      1 ,   1
                withdrawal ,      1 ,   3 , 50.0
                chargeback ,      1 ,   2
                deposit    ,      2 ,   11 , 100.0
                deposit    ,      2 ,   12 , 300.0
                dispute    ,      2 ,   11
                withdrawal ,      2 ,   13 , 50.0
                deposit    ,      2 ,   14 , 150.0
                resolve    ,      2 ,   11
                dispute    ,      2 ,   14";

            let reader = ReaderBuilder::new()
                .trim(csv::Trim::All)
                .flexible(true)
                .from_reader(data.as_bytes());

            assert_eq!(read_all(reader, &sender).is_ok(), true);
        });

        let mut p = PaymentEngine::new();
        p.process_all(receiver);

        let mut writer = Cursor::new(Vec::new());
        p.generate_report(&mut writer, true) // lets sort the outout by client
            .expect("cannot write to stdout");

        t.join().unwrap();

        let output = String::from_utf8(writer.into_inner()).expect("convert Vec<u8> into String");
        let expected = "client, available, held, total, locked
     1,       250,  100,   350,  false
     2,       350,  150,   500,  false
";
        assert_eq!(output, expected.to_string());
    }
}
