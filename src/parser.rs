use std::{
    io,
    result::Result,
    sync::mpsc::{SendError, Sender},
};

use log::{error, info};
use serde::Deserialize;

#[derive(Debug, PartialEq, Clone)]
pub enum Transaction {
    Deposit(TransactionDirect),
    Withdrawal(TransactionDirect),
    Dispute(TransactionIndirect),
    Resolve(TransactionIndirect),
    Chargeback(TransactionIndirect),
}

#[derive(Debug, PartialEq, Clone)]
pub struct TransactionDirect {
    pub client: u16,
    pub id: u32,
    pub amount: f64, // Decimal,
}

#[derive(Debug, PartialEq, Clone)]
pub struct TransactionIndirect {
    pub client: u16,
    pub id: u32,
}

#[derive(Debug, Deserialize)]
enum TransactionType {
    #[serde(rename = "deposit")]
    Deposit,
    #[serde(rename = "withdrawal")]
    Withdrawal,
    #[serde(rename = "dispute")]
    Dispute,
    #[serde(rename = "resolve")]
    Resolve,
    #[serde(rename = "chargeback")]
    Chargeback,
}

#[derive(Debug, Deserialize)]
struct RawTransaction {
    r#type: TransactionType,
    client: u16,
    tx: u32,
    amount: Option<f64>,
}

#[derive(Debug)]
struct RawTransactionParseError(String);

impl TryFrom<RawTransaction> for Transaction {
    type Error = RawTransactionParseError;

    fn try_from(raw: RawTransaction) -> Result<Transaction, Self::Error> {
        use TransactionType::*;
        Ok(match raw.r#type {
            Deposit => Transaction::Deposit(TransactionDirect {
                client: raw.client,
                id: raw.tx,
                amount: raw.amount.ok_or_else(|| {
                    RawTransactionParseError(format!("amount missing from {:?}", raw))
                })?,
            }),
            Withdrawal => Transaction::Withdrawal(TransactionDirect {
                client: raw.client,
                id: raw.tx,
                amount: raw.amount.ok_or_else(|| {
                    RawTransactionParseError(format!("amount missing from {:?}", raw))
                })?,
            }),
            Dispute => Transaction::Dispute(TransactionIndirect {
                client: raw.client,
                id: raw.tx,
            }),
            Resolve => Transaction::Resolve(TransactionIndirect {
                client: raw.client,
                id: raw.tx,
            }),
            Chargeback => Transaction::Chargeback(TransactionIndirect {
                client: raw.client,
                id: raw.tx,
            }),
        })
    }
}

pub fn read_all<R: io::Read>(
    reader: csv::Reader<R>,
    sender: &Sender<Transaction>,
) -> Result<(), SendError<Transaction>> {
    // here reader does not load the entire file into memory,
    // instead it returns an iterator which reads the data as we
    // consume it.
    for item in reader.into_deserialize() {
        match item {
            Ok(t) => {
                let tx: RawTransaction = t;
                match tx.try_into() {
                    Ok(tx) => {
                        info!("tx: {:?}", tx);
                        sender.send(tx)?;
                    }
                    Err(e) => {
                        error!("RawTransaction to Transaction conversion failed: {:?}", e);
                    }
                }
            }
            Err(e) => {
                error!("Deserialization into RawTransaction failed: {:?}", e);
            }
        }
    }
    Ok(())
}

pub fn read_csv_files(
    files: &[&str],
    sender: Sender<Transaction>,
) -> Result<(), SendError<Transaction>> {
    for file in files {
        let reader = csv::ReaderBuilder::new()
            .trim(csv::Trim::All) // trim spaces
            .flexible(true) // some columns might be omitted
            .from_path(file)
            .unwrap();
        read_all(reader, &sender)?;
    }
    Ok(())
}
