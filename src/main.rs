use std::{env, io, sync::mpsc::channel, thread};

use log::{error, info};
use simplelog::{ColorChoice, ConfigBuilder, LevelFilter, TermLogger, TerminalMode};

use payments::{parser::read_csv_files, PaymentEngine};

fn init_loggger() {
    TermLogger::init(
        LevelFilter::Debug,
        ConfigBuilder::new()
            .set_thread_level(LevelFilter::Off)
            .build(),
        TerminalMode::Stderr,
        ColorChoice::Auto,
    )
    .expect("TermLogger::int() failed");
}

fn main() {
    let args: Vec<String> = env::args().collect();
    if args.len() < 2 {
        println!("Usage: \n");
        println!("\t$ cargo run -- <csv-file> [<csv-file> ... ]\n");
        println!("\t$ ./payments <csv-file> [<csv-file> ... ]\n");
        return;
    }

    init_loggger();

    let (sender, receiver) = channel();

    let t = thread::spawn(move || {
        let args: Vec<&str> = args.iter().skip(1).map(AsRef::as_ref).collect();

        if let Err(e) = read_csv_files(&args, sender) {
            error!("channel disconnected: {:?}", e);
        }
    });

    let mut p = PaymentEngine::new();
    p.process_all(receiver);

    p.generate_report(&mut io::stdout(), false)
        .expect("cannot write to stdout");

    t.join().unwrap();

    info!("Done");
}
